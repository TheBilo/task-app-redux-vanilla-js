const initialState = {
  count : 0
}

const reducer = (state = initialState, action) => {
  const { count } = state;

  switch(action.type){
    case 'Add':
      return {
        ...state,
        count : count + 1
      }
    case 'Take':
      return {
        ...state,
        count : (count > 0) ? count - 1 : count
      }

    default:
      return state;
  }
}

export default reducer;