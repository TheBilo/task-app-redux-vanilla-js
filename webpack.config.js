const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssWebpackPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry : './src/js/index.js',
  output : {
    path : __dirname + '/build',
    filename : 'bundle.js'
  },
  module : {
    rules : [
      {
        test : /\.scss$/i,
        use : [MiniCssExtractPlugin.loader,'css-loader','sass-loader']
      }
    ]
  },
  devServer : {
    port : 5000
  },
  plugins : [
    new HtmlWebpackPlugin({
      template : './src/index.html'
    }),
    new CssWebpackPlugin({
      filename : 'bundle.css'
    })
  ]
}